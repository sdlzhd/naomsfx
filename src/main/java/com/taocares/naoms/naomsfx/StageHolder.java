package com.taocares.naoms.naomsfx;

import javafx.stage.Stage;

public interface StageHolder {
    void setStage(Stage stage);
}
