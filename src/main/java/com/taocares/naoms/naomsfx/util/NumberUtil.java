package com.taocares.naoms.naomsfx.util;

public final class NumberUtil {
    public static int max(int... nums) {
        int max = nums[0];
        for (int i : nums) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    public static double max(double... nums) {
        double max = nums[0];
        for (double i : nums) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    public static float max(float... nums) {
        float max = nums[0];
        for (float i : nums) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }
}
