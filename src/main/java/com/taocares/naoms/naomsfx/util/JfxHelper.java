package com.taocares.naoms.naomsfx.util;

public class JfxHelper {

    private static String path;

    public static String getStylesheetsPath() {
        if (path == null) {
            path = JfxHelper.class.getResource("/default.css").toString();
        }
        return path;
    }
}
