package com.taocares.naoms.naomsfx.control.behavior;

import com.sun.javafx.scene.control.behavior.BehaviorBase;
import com.sun.javafx.scene.control.behavior.KeyBinding;
import com.taocares.naoms.naomsfx.control.SearchBox;

import java.util.List;

public class SearchBoxBehavior extends BehaviorBase<SearchBox> {
    /**
     * Create a new BehaviorBase for the given control. The Control must not
     * be null.
     *
     * @param control     The control. Must not be null.
     * @param keyBindings The key bindings that should be used with this behavior.
     */
    public SearchBoxBehavior(SearchBox control, List<KeyBinding> keyBindings) {
        super(control, keyBindings);
    }
}
