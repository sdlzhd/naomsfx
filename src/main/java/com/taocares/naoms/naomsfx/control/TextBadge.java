package com.taocares.naoms.naomsfx.control;

import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.scene.control.Label;

/**
 * 带有文本Badge
 */
public class TextBadge extends Badge<Label> {

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/
    public TextBadge() {
        badgeLabel.textProperty().bind(valueProperty());
    }

    private final Label badgeLabel = new Label();

    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/
    private StringProperty valueProperty;

    public String getValue() {
        return valueProperty().get();
    }

    @Override
    public Label definitionBadgeRegion() {
        return badgeLabel;
    }

    public void setValue(String value) {
        valueProperty().set(value);
    }

    public StringProperty valueProperty() {
        if (valueProperty == null) {
            valueProperty = new StringPropertyBase() {

                @Override
                public Object getBean() {
                    return TextBadge.this;
                }

                @Override
                public String getName() {
                    return "value";
                }
            };
        }
        return valueProperty;
    }
}
