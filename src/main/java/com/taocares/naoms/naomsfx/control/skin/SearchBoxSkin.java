package com.taocares.naoms.naomsfx.control.skin;

import com.taocares.naoms.naomsfx.control.SearchBox;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.AccessibleRole;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;

public class SearchBoxSkin extends SkinBase<SearchBox> {

    private TextField searchText;
    private StackPane searchBtn;
    private StackPane container;

    public SearchBoxSkin(SearchBox control) {
        super(control);
        initLayout();
        control.textProperty().bind(searchText.textProperty());
        searchText.promptTextProperty().bind(getSkinnable().placeholderProperty());
        searchText.promptTextProperty().bind(control.placeholderProperty());
    }

    private void initLayout() {
        searchText = new TextField();
        searchBtn = new StackPane();
        searchBtn.setAccessibleRole(AccessibleRole.BUTTON);
        container = new StackPane() {
            @Override
            protected void layoutChildren() {

                final double left = SearchBoxSkin.this.snappedLeftInset();

                final double w = getWidth();
                final double h = getHeight();

                final double searchBtnWidth = snapSize(searchBtn.prefWidth(-1));
                final double searchBtnHeight = snapSize(searchBtn.prefHeight(-1));
                final double searchTextWidth = w - searchBtnWidth - left;
                final double searchTextHeight = snapSize(searchText.prefHeight(-1));

                final double searchBtnStartX = w - searchBtnWidth;
                final double searchBenStartY = (h - searchBtnHeight) / 2;
                final double searchTextStartY = (h - searchTextHeight) / 2;

                layoutInArea(searchBtn, searchBtnStartX, searchBenStartY, searchBtnWidth, searchBtnHeight, 0, HPos.CENTER, VPos.CENTER);

                searchText.resize(searchTextWidth, h);
                positionInArea(searchText, left, searchTextStartY, searchTextWidth, searchTextHeight, 0, HPos.CENTER, VPos.CENTER);

            }
        };
        searchText.getStyleClass().add("search-text");
        searchBtn.getStyleClass().add("search-btn");
        container.getStyleClass().add("container");
        container.getChildren().addAll(searchText, searchBtn);
        getChildren().add(container);
    }
}
