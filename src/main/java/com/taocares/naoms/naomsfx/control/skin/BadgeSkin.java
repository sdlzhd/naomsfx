package com.taocares.naoms.naomsfx.control.skin;

import com.taocares.naoms.naomsfx.control.Badge;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class BadgeSkin extends SkinBase<Badge> {

    private final StackPane container;
    private final Region badgeRegion;
    private final Node slot;

    /**
     * Constructor for all SkinBase instances.
     *
     * @param control The control for which this Skin should attach to.
     */
    public BadgeSkin(Badge control) {
        super(control);
        slot = control.getSlot();
        badgeRegion = control.definitionBadgeRegion();
        container = new StackPane() {
            @Override
            protected void layoutChildren() {
                super.layoutChildren();
                final double w = container.getWidth();

                double regionWidth;
                double regionHeight;
                double regionStartX;
                double regionStartY;

                if (control.isShow()) {
                    regionWidth = badgeRegion.prefWidth(-1);
                    regionHeight = badgeRegion.prefHeight(-1);
                    regionStartX = w - regionWidth / 2;
                    regionStartY = -regionHeight / 2;
                } else {
                    regionWidth = 0;
                    regionHeight = 0;
                    regionStartX = 0;
                    regionStartY = 0;
                }
                badgeRegion.resize(regionWidth, regionHeight);
                positionInArea(badgeRegion, regionStartX, regionStartY, regionWidth, regionHeight, 0, HPos.CENTER, VPos.CENTER);
            }
        };
        container.getChildren().addAll(slot, badgeRegion);
        getChildren().add(container);
    }

    @Override
    protected double computePrefWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
        return slot.prefWidth(-1);
    }

    @Override
    protected double computeMaxWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
        return slot.prefWidth(-1);
    }

    @Override
    protected double computePrefHeight(double width, double topInset, double rightInset, double bottomInset, double leftInset) {
        return slot.prefHeight(-1);
    }

    @Override
    protected double computeMaxHeight(double width, double topInset, double rightInset, double bottomInset, double leftInset) {
        return slot.prefHeight(-1);
    }
}
