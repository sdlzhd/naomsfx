package com.taocares.naoms.naomsfx.control;

import com.taocares.naoms.naomsfx.control.skin.SearchBoxSkin;
import javafx.beans.property.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

/**
 * 搜索框，
 */
public class SearchBox extends Control {

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    public SearchBox() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }

    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    /**
     * 待搜索的文本内容，将在触发搜索事件的时候，作为关键字传入
     */
    private StringProperty textProperty;

    /**
     * 当待搜索的文本内容为空的时候，显示的占位文本，例如“请输入”
     * <p>
     * 默认值为空
     */
    private StringProperty placeholderProperty;

    /**
     * 是否允许使用回车触发搜索事件
     * <p>
     * 默认为true
     */
    private BooleanProperty allowTriggeringByEntry;

    /**
     * 搜索事件，将在点击搜索按钮或回车（如果允许）的时候触发
     */
    private ObjectProperty<EventHandler<ActionEvent>> onSearch;

    /***************************************************************************
     *                                                                         *
     * Stylesheet Handling                                                     *
     *                                                                         *
     **************************************************************************/

    private static final String DEFAULT_STYLE_CLASS = "search-box";

    @Override
    public String getUserAgentStylesheet() {
        return SearchBox.class.getResource("/css/search-box.css").toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new SearchBoxSkin(this);
    }

    public String getText() {
        return textProperty().get();
    }

    public final void setText(String value) {
        textProperty().set(value);
    }

    public final StringProperty textProperty() {
        if (textProperty == null) {
            textProperty = new StringPropertyBase() {
                @Override
                public Object getBean() {
                    return SearchBox.this;
                }

                @Override
                public String getName() {
                    return "text";
                }
            };
        }
        return textProperty;
    }

    public final String getPlaceholder() {
        return placeholderProperty().get();
    }

    public final void setPlaceholder(String value) {
        placeholderProperty().set(value);
    }

    public final StringProperty placeholderProperty() {
        if (placeholderProperty == null) {
            placeholderProperty = new StringPropertyBase() {
                @Override
                public Object getBean() {
                    return SearchBox.this;
                }

                @Override
                public String getName() {
                    return "placeholder";
                }
            };
        }
        return placeholderProperty;
    }

    public final boolean isAllowTriggeringByEntry() {
        return allowTriggeringByEntry().get();
    }

    public final void setAllowTriggeringByEntry(boolean value) {
        allowTriggeringByEntry().set(value);
    }

    public final BooleanProperty allowTriggeringByEntry() {
        if (allowTriggeringByEntry == null) {
            allowTriggeringByEntry = new BooleanPropertyBase(true) {
                @Override
                public Object getBean() {
                    return SearchBox.this;
                }

                @Override
                public String getName() {
                    return "allowTriggeringByEntry";
                }
            };
        }
        return allowTriggeringByEntry;
    }

    public final void setonSearch(EventHandler<ActionEvent> value) {
        onActionProperty().set(value);
    }

    public final EventHandler<ActionEvent> getonSearch() {
        return onActionProperty().get();
    }

    public final ObjectProperty<EventHandler<ActionEvent>> onActionProperty() {
        if (onSearch == null) {
            onSearch = new ObjectPropertyBase<EventHandler<ActionEvent>>() {

                @Override
                public Object getBean() {
                    return SearchBox.this;
                }

                @Override
                public String getName() {
                    return "onAction";
                }
            };
        }
        return onSearch;
    }
}
