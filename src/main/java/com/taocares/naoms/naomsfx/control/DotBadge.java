package com.taocares.naoms.naomsfx.control;

import javafx.scene.layout.StackPane;

/**
 * 小红点Badge
 */
public class DotBadge extends Badge<StackPane> {

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    public DotBadge() {
        badgeRegion.getStyleClass().add("dot");
    }

    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    private StackPane badgeRegion = new StackPane();

    @Override
    public StackPane definitionBadgeRegion() {
        return badgeRegion;
    }
}
