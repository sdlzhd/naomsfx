package com.taocares.naoms.naomsfx.control;

import com.taocares.naoms.naomsfx.control.skin.BadgeSkin;
import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.scene.AccessibleRole;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.layout.Region;

/**
 * @param <T> Region以及Region的子类，用来作为Badge的显示点
 */
@DefaultProperty("slot")
public abstract class Badge<T extends Region> extends Control {

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/
    public Badge() {
        this(null);
    }

    public Badge(Region slot) {
        setSlot(slot);
        initialize();
    }

    private void initialize() {
        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
        setAccessibleRole(AccessibleRole.NODE);
    }

    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    /**
     * Badge的插槽，Badge将显示在该插槽内容的右上角
     */
    private ObjectProperty<Region> slotProperty;

    /**
     * 设置Badge是否被显示
     */
    private BooleanProperty showProperty;

    /***************************************************************************
     *                                                                         *
     * Stylesheet Handling                                                     *
     *                                                                         *
     **************************************************************************/

    private static final String DEFAULT_STYLE_CLASS = "badge";

    @Override
    protected Skin<Badge> createDefaultSkin() {
        return new BadgeSkin(this);
    }

    @Override
    public String getUserAgentStylesheet() {
        return Badge.class.getResource("/css/badge.css").toExternalForm();
    }

    /**
     * 返回一个Region对象，该对象将用来表示Badge
     * <p>
     * Region应该自行实现内部数据的管理
     */
    public abstract T definitionBadgeRegion();

    public final void setSlot(Region region) {
        slotProperty().set(region);
    }

    public final Region getSlot() {
        return slotProperty().get();
    }

    public final ObjectProperty<Region> slotProperty() {
        if (slotProperty == null) {
            slotProperty = new ObjectPropertyBase<Region>() {
                @Override
                public Object getBean() {
                    return Badge.this;
                }

                @Override
                public String getName() {
                    return "slot";
                }
            };
        }
        return slotProperty;
    }

    public final boolean isShow() {
        return showProperty().get();
    }

    public final void setShow(boolean value) {
        showProperty().set(value);
    }

    public final BooleanProperty showProperty() {
        if (showProperty == null) {
            showProperty = new BooleanPropertyBase(false) {
                @Override
                public Object getBean() {
                    return Badge.this;
                }

                @Override
                public String getName() {
                    return "show";
                }
            };
        }
        return showProperty;
    }

}
