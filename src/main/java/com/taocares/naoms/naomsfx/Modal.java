package com.taocares.naoms.naomsfx;

import com.taocares.naoms.naomsfx.util.JfxHelper;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * 模态对话框
 */
public final class Modal extends Stage {

    private static Stage defaultStage;

    public static void setDefaultStage(Stage stage) {
        Modal.defaultStage = stage;
    }

    private static final String styleClass = "modal";

    private BorderPane root = new BorderPane();
    private AnchorPane titleBar = new AnchorPane();
    private HBox footerBar = new HBox();

    private Pane content;

    /**
     * 弹窗标题
     */
    private String title;

    private double mouseDragOffsetX = 0;
    private double mouseDragOffsetY = 0;

    public Modal(Builder builder) {
        if (builder.getTitle() == null) {
            this.title = "";
        } else {
            this.title = builder.getTitle();
        }
        if (builder.getWidth() > 0) {
            setWidth(builder.getWidth());
        }
        if (builder.getHeight() > 0) {
            setHeight(builder.getHeight());
        }
        // ESC关闭窗口
        if (!builder.isDisableESC()) {
            addEventFilter(KeyEvent.KEY_PRESSED, e -> this.close());
        }
        if (builder.getData() != null) {
            setUserData(builder.getData());
        }
        this.content = builder.getContent();
        initOwner(builder.getOwner());
        init();
    }

    private void init() {
        /* 控件声明 */
        // 标题
        Label titleLabel = new Label(title);
        // 关闭按钮
        Region close = new Region();
        // 控制区域
        StackPane controlPane = new StackPane();
        /* 样式声明 */
        root.getStyleClass().add(styleClass);
        titleBar.getStyleClass().add("modal-title-bar");
        titleLabel.getStyleClass().add("modal-title");
        controlPane.getStyleClass().add("modal-control-pane");
        controlPane.setId("abc");
        close.getStyleClass().add("modal-close-btn");
        content.getStyleClass().add("content");
        /* 事件声明 */
        // 窗口拖拽
        titleBar.setOnMousePressed(e -> {
            mouseDragOffsetX = e.getSceneX();
            mouseDragOffsetY = e.getSceneY();
        });
        titleBar.setOnMouseDragged(e -> {
            if (!this.isMaximized()) {
                this.setX(e.getScreenX() - mouseDragOffsetX);
                this.setY(e.getScreenY() - mouseDragOffsetY);
            }
        });
        // 控制区域不可拖拽
        controlPane.setOnMousePressed(Event::consume);
        controlPane.setOnMouseDragged(Event::consume);
        controlPane.setOnMouseClicked(e -> this.close());
        /* 界面组织 */
        // 标题栏
        root.setTop(titleBar);
        titleBar.getChildren().addAll(titleLabel, controlPane);
        AnchorPane.setLeftAnchor(titleLabel, 10.0);
        AnchorPane.setTopAnchor(titleLabel, 5.0);
        AnchorPane.setRightAnchor(controlPane, 0.0);
        controlPane.getChildren().add(close);
        // 底部栏
        root.setBottom(footerBar);
        // 内容区域
        root.setCenter(content);
        /* 场景声明 */
        Scene scene = new Scene(root);
        scene.getStylesheets().add(JfxHelper.getStylesheetsPath());
        // 设置模态
        initModality(Modality.APPLICATION_MODAL);
        // 设置Stage样式
        initStyle(StageStyle.UNDECORATED);
        // 窗口不可拉伸
        setResizable(false);
        setScene(scene);
    }

    /**
     * 模态对话框构建器
     */
    public static final class Builder {

        private int width;
        private int height;
        private String title;
        private Stage owner;
        private Pane content;

        private boolean disableESC;

        private Object data;

        public Builder() {
            width = -1;
            height = -1;
            title = "";
            owner = Modal.defaultStage;

            disableESC = false;

            data = null;
        }

        public Builder width(int width) {
            this.width = width;
            return this;
        }

        public Builder height(int height) {
            this.height = height;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder disableCloseByESC() {
            this.disableESC = true;
            return this;
        }

        public Builder owner(Stage owner) {
            this.owner = owner;
            return this;
        }

        public Builder setData(Object data) {
            this.data = data;
            return this;
        }

        public Modal loadContent(Pane content) {
            this.content = content;
            Modal modal = new Modal(this);
            if (content instanceof StageHolder) {
                ((StageHolder) content).setStage(modal);
            }
            return modal;
        }

        public Modal loadContent(String fxmlPath) {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(fxmlPath));
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            Modal modal = null;
            try {
                this.content = fxmlLoader.load();
                modal = new Modal(this);
                if (fxmlLoader.getController() instanceof StageHolder) {
                    ((StageHolder) fxmlLoader.getController()).setStage(modal);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return modal;
        }

        int getWidth() {
            return width;
        }

        int getHeight() {
            return height;
        }

        String getTitle() {
            return title;
        }

        Stage getOwner() {
            return owner;
        }

        Pane getContent() {
            return content;
        }

        boolean isDisableESC() {
            return disableESC;
        }

        Object getData() {
            return data;
        }
    }
}
