package com.taocares.naoms.naomsfx.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class ListViewController implements Initializable {

    @FXML
    private ListView<String> lv_default;
    @FXML
    private ListView<String> lv_box;
    @FXML
    private ListView<String> lv_block;
    @FXML
    private Text t_default;
    @FXML
    private Text t_box;
    @FXML
    private Text t_block;

    private ObservableList<String> itemList = FXCollections.observableArrayList("One", "Two", "Three", "Four", "Five");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lv_default.setItems(itemList);
        lv_box.setItems(itemList);
        lv_block.setItems(itemList);
        lv_default.getSelectionModel().select(0);
        lv_box.getSelectionModel().select(0);
        lv_block.getSelectionModel().select(0);
        ExampleController.setWrappingWidthForText(t_default, t_box, t_block);
    }
}
