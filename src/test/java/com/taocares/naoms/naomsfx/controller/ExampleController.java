package com.taocares.naoms.naomsfx.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ExampleController implements Initializable {

    @FXML
    private ListView<String> lv_controls;
    @FXML
    private Tab tab_preview;
    @FXML
    private Tab tab_code;

    private ObservableList<String> controlList = FXCollections.observableArrayList(
            "Button",
            "Form",
            "ListView",
            "TableView",
            "TreeView",
            "Others",
            "Badge",
            "SearchBox"
    );

    public void initialize(URL location, ResourceBundle resources) {
        lv_controls.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                tab_preview.setContent(loadPreviewPaneFromFXML(newValue));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        lv_controls.setItems(controlList);
        lv_controls.getSelectionModel().select(0);
    }

    private Pane loadPreviewPaneFromFXML(String fxmlName) throws IOException {
        return FXMLLoader.load(getClass().getResource("/fxml/tab/" + fxmlName + ".fxml"));
    }

    public static void setWrappingWidthForText(Text... texts) {
        for (Text text : texts) {
            text.setWrappingWidth(620);
        }
    }
}
