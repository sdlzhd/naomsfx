package com.taocares.naoms.naomsfx.controller;

import com.taocares.naoms.naomsfx.Modal;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class ButtonController implements Initializable {

    @FXML
    private ScrollPane sp_root;

    @FXML
    private Text t_default;
    @FXML
    private Text t_radius;
    @FXML
    private Text t_nobg;
    @FXML
    private Text t_small;
    @FXML
    private Text t_segment;
    @FXML
    private Text t_icon;
    @FXML
    private Text t_icon_tips;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ExampleController.setWrappingWidthForText(t_default, t_radius, t_nobg, t_small, t_segment, t_icon, t_icon_tips);
    }

    @FXML
    public void openModal() {
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(new Button("hi"));
        ButtonController controller = null;
        new Modal.Builder()
                .title("测试弹窗")
                .setData("嘻嘻嘻")
                .loadContent("/fxml/tab/TableView.fxml")
                .show();
    }
}
