package com.taocares.naoms.naomsfx.controller;

import com.taocares.naoms.naomsfx.StageHolder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class TableViewController implements Initializable, StageHolder {

    private Stage stage;

    @FXML
    private TableView<User> tv_default;

    @FXML
    private Text t_tableView;

    private TableColumn<User, String> tv_defaultColumnForName = new TableColumn<>("Name");
    private TableColumn<User, String> tv_defaultColumnForAge = new TableColumn<>("Age");
    private TableColumn<User, String> tv_defaultColumnForSex = new TableColumn<>("Sex");
    private TableColumn<User, String> tv_defaultColumnForSchool = new TableColumn<>("School");
    private TableColumn<User, String> tv_defaultColumnForMajor = new TableColumn<>("Major");
    private TableColumn<User, String> tv_defaultColumnForGrade = new TableColumn<>("Grade");

    private ObservableList<User> userList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tv_defaultColumnForName.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
        tv_defaultColumnForAge.setCellValueFactory(new PropertyValueFactory<User, String>("age"));
        tv_defaultColumnForSex.setCellValueFactory(new PropertyValueFactory<User, String>("sex"));
        tv_defaultColumnForSchool.setCellValueFactory(new PropertyValueFactory<User, String>("school"));
        tv_defaultColumnForMajor.setCellValueFactory(new PropertyValueFactory<User, String>("major"));
        tv_defaultColumnForGrade.setCellValueFactory(new PropertyValueFactory<User, String>("grade"));

        tv_default.getColumns().addAll(
                tv_defaultColumnForName,
                tv_defaultColumnForAge,
                tv_defaultColumnForSex,
                tv_defaultColumnForSchool,
                tv_defaultColumnForMajor,
                tv_defaultColumnForGrade);

        userList.addAll(
                new User("东东", "15", "男", "青岛科技大学", "软件工程", "14"),
                new User("西西", "15", "女", "青岛科技大学", "软件工程", "14"),
                new User("南南", "15", "男", "青岛科技大学", "软件工程", "14"),
                new User("北北", "15", "女", "青岛科技大学", "软件工程", "14")
        );

        tv_default.setItems(userList);


        ExampleController.setWrappingWidthForText(t_tableView);
        tv_default.setOnMouseEntered(e->{
            System.err.println(stage.getUserData());
        });
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public class User {
        private String name;
        private String age;
        private String sex;
        private String school;
        private String major;
        private String grade;

        public User(String name, String age, String sex, String school, String major, String grade) {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.school = school;
            this.major = major;
            this.grade = major;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getSchool() {
            return school;
        }

        public void setSchool(String school) {
            this.school = school;
        }

        public String getMajor() {
            return major;
        }

        public void setMajor(String major) {
            this.major = major;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }
    }
}
