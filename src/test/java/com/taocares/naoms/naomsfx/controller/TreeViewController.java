package com.taocares.naoms.naomsfx.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.net.URL;
import java.util.ResourceBundle;

public class TreeViewController implements Initializable {

    @FXML
    private TreeView<String> treeView;

    private TreeItem<String> treeItem = new TreeItem<>("root");

    private TreeItem<String> treeItem1 = new TreeItem<>("treeItem1");
    private TreeItem<String> treeItem2 = new TreeItem<>("treeItem2");
    private TreeItem<String> treeItem3 = new TreeItem<>("treeItem3");

    private TreeItem<String> treeItem1_1 = new TreeItem<>("treeItem1_1");
    private TreeItem<String> treeItem1_2 = new TreeItem<>("treeItem1_2");
    private TreeItem<String> treeItem1_3 = new TreeItem<>("treeItem1_3");

    private TreeItem<String> treeItem3_1 = new TreeItem<>("treeItem3_1");
    private TreeItem<String> treeItem3_2 = new TreeItem<>("treeItem3_2");


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        treeItem1.getChildren().addAll(treeItem1_1, treeItem1_2, treeItem1_3);
        treeItem3.getChildren().addAll(treeItem3_1, treeItem3_2);
        treeItem.getChildren().addAll(treeItem1, treeItem2, treeItem3);
        treeView.setRoot(treeItem);
    }
}
