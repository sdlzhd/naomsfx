package com.taocares.naoms.naomsfx.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class FormController implements Initializable {

    @FXML
    private Text t_textField;
    @FXML
    private Text t_textArea;
    @FXML
    private Text t_spinner;
    @FXML
    private Text t_radioButton;
    @FXML
    private Text t_checkBox;
    @FXML
    private Text t_comboBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ExampleController.setWrappingWidthForText(t_textField, t_textArea, t_spinner, t_radioButton, t_checkBox, t_comboBox);
    }
}
