package com.taocares.naoms.naomsfx;

import com.taocares.naoms.naomsfx.util.JfxHelper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/example.fxml"));
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(JfxHelper.getStylesheetsPath());
        primaryStage.setScene(scene);
        primaryStage.setWidth(900);
        primaryStage.setHeight(600);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

}
